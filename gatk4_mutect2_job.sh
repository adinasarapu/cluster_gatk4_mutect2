#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D			#
# Emory University, Dept. of Human Genetics	#
# ashok.reddy.dinasarapu@emory.edu		#
# Date: 03/07/2019				#
#################################################

# https://gatkforums.broadinstitute.org/gatk/discussion/11127

echo "Start - `date`"

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
 TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

module load R/3.4.3
module load tabix/0.2.6

PICARD="java -Xmx8G -jar /sw/hgcc/Pkgs/picardtools/2.6.0/picard.jar"
GATK_DATA_DIR=/sw/hgcc/Data/GATK/4.0/ftp.broadinstitute.org/bundle
GENOME_DIR=~/iGenomes/Homo_sapiens/UCSC/hg19

# create sub dirs in tmp directory
chmod a+trwx $TMPDIR
/usr/bin/mkdir -p $TMPDIR/{bam,ref,vcf}

################################################################
# Data upload: bam, Ref Genome, Exome Intervals, dbSNP, HAPMAP #
################################################################

## @RG	ID:GHN_85_R1_001.fastq	PU:Unknown	LB:ClearSeq Comprehensive Cancer	SM:GHN_85_R1_001.fastq	PL:Illumina

rsync -arv $R $TMPDIR/bam/${SID}.bam
rsync -arv $DATA_HOME/surecall_bam/test/V3_Exons.bed $TMPDIR/bam/exome_intervals.list.bed
rsync -arv --exclude=Bisulfite_Genome $GENOME_DIR/Sequence/WholeGenomeFasta/* $TMPDIR/ref/

rsync -arv $GATK_DATA_DIR/hg19/dbsnp_138.hg19.vcf.gz $TMPDIR/vcf/
rsync -arv $GATK_DATA_DIR/hg19/hapmap_3.3.hg19.sites.vcf.gz $TMPDIR/vcf/
# rsync -av $GATK_DATA_DIR/hg19/ucsc.hg19.fasta.gz $TMPDIR/vcf/
# rsync -av $GATK_DATA_DIR/hg19/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf.gz $TMPDIR/vcf/

# convert gzip'ed to bgzip'ed file
gunzip $TMPDIR/vcf/dbsnp_138.hg19.vcf.gz
bgzip $TMPDIR/vcf/dbsnp_138.hg19.vcf
tabix -p vcf $TMPDIR/vcf/dbsnp_138.hg19.vcf.gz

gunzip $TMPDIR/vcf/hapmap_3.3.hg19.sites.vcf.gz
bgzip $TMPDIR/vcf/hapmap_3.3.hg19.sites.vcf
tabix -p vcf $TMPDIR/vcf/hapmap_3.3.hg19.sites.vcf.gz

# tabix -p vcf $TMPDIR/vcf/ucsc.hg19.fasta.gz
# tabix -p vcf $TMPDIR/vcf/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf.gz

########################################################################
# Data upload: ExAC - Mutect2.variants_for_contamination -- (optional) #
########################################################################

# Exome Aggregation Consortium (ExAC)

# VCF containing population allele frequencies (AF) of common SNPs 
# to calculate cross-sample contamination and contamination filtering. 

# This can also be generated from a gnomAD VCF using the GATK4 tool 
# SelectVariants with the argument --select "AF > 0.05". 

rsync -arv $GATK_DATA_DIR/Mutect2/GetPileupSummaries/small_exac_common_3_b37.vcf.gz  $TMPDIR/vcf/
gunzip $TMPDIR/vcf/small_exac_common_3_b37.vcf.gz

###########################################
# Data upload: gnomAD - Germline_resource #
###########################################

# Population VCF of germline sequencing containing allele fractions.
# gnomAD VCF containing population allele frequencies (AF) of common and rare alleles. 

rsync -arv $GATK_DATA_DIR/Mutect2/af-only-gnomad.raw.sites.b37.vcf.gz $TMPDIR/vcf/
gunzip $TMPDIR/vcf/af-only-gnomad.raw.sites.b37.vcf.gz

#######################
# LiftOver chain file #
#######################

rsync -arv $DATA_HOME/scripts/b37tohg19.chain $TMPDIR/vcf/ 

################
# LiftOver VCF #
################

$PICARD LiftoverVcf \
 I=$TMPDIR/vcf/af-only-gnomad.raw.sites.b37.vcf \
 O=$TMPDIR/vcf/af-only-gnomad.raw.sites.hg19.vcf \
 CHAIN=$TMPDIR/vcf/b37tohg19.chain \
 REJECT=$TMPDIR/vcf/af-only-gnomad.rejected.sites.b37.vcf \
 R=$TMPDIR/ref/genome.fa
 
$PICARD LiftoverVcf \
 I=$TMPDIR/vcf/small_exac_common_3_b37.vcf \
 O=$TMPDIR/vcf/small_exac_common_3_hg19.vcf \
 CHAIN=$TMPDIR/vcf/b37tohg19.chain \
 REJECT=$TMPDIR/vcf/small_exac_rejected_3_b37.vcf \
 R=$TMPDIR/ref/genome.fa

##############################
# Add Or Replace Read Groups #
##############################

# Provide the BAM with -I and the sample's read group sample name (the SM field value) with -tumor
# Use samtools for SM value : samtools view -H tumor.bam | grep '@RG'

# Add RGSM
##$PICARD AddOrReplaceReadGroups \
# I=$TMPDIR/bam/${SID}.dedupe.bam \
# O=$TMPDIR/bam/${SID}.SM.bam \
# RGID=ID_$SID \
# RGPL=illumina \
# RGLB=clearseq \
# RGSM=SM_${SID} \
# RGPU=RG_${SID}

####################################################################
# 1. Estimate extent of FFPE and OxoG artifacts                    #
####################################################################

# Collect metrics to quantify single-base sequencing artifacts 
# CollectSequencingArtifactMetrics only needs to run on the tumor sample
 
#--INPUT,-I:File               Input SAM or BAM file.  Required. 
#--OUTPUT,-O:File              File to write the output to.  Required. 
#--REFERENCE_SEQUENCE,-R:File  Reference sequence file.  Required.
 
# Also available from PICARD
# gatk --java-options "-Xmx8G" CollectSequencingArtifactMetrics \
# -I $TMPDIR/bam/${SID}.recal.bam \
# -O $TMPDIR/bam/${SID}.recal.artifact_metrics.txt \
# -R $TMPDIR/ref/genome.fa
 
$PICARD CollectSequencingArtifactMetrics \
 I=$TMPDIR/bam/${SID}.bam \
 O=$TMPDIR/bam/${SID}.artifact_metrics.txt \
 R=$TMPDIR/ref/genome.fa

##############################################
# 2. Base Quality Score Recalibration (BQSR) #
##############################################

# Detect systematic errors in base quality scores

# first the program builds a model of covariation based on the data and a set of known variants, 
# then it adjusts the base quality scores in the data based on the model. 

# known-sites: A database of known polymorphic sites to mask out.

#  --known-sites $TMPDIR/vcf/ucsc.hg19.fasta.gz \
#  --known-sites $TMPDIR/vcf/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf.gz \

gatk --java-options "-Xmx8G" BaseRecalibrator \
 -R $TMPDIR/ref/genome.fa \
 -I $TMPDIR/bam/${SID}.bam \
 --known-sites $TMPDIR/vcf/dbsnp_138.hg19.vcf.gz \
 --known-sites $TMPDIR/vcf/hapmap_3.3.hg19.sites.vcf.gz \
 -O $TMPDIR/bam/${SID}_recal_data.table1

gatk --java-options "-Xmx8G" ApplyBQSR \
 -R $TMPDIR/ref/genome.fa \
 -bqsr $TMPDIR/bam/${SID}_recal_data.table1 \
 -I $TMPDIR/bam/${SID}.bam \
 -O $TMPDIR/bam/${SID}.recal.bam

gatk --java-options "-Xmx8G" BaseRecalibrator \
 -R $TMPDIR/ref/genome.fa \
 -I $TMPDIR/bam/${SID}.recal.bam \
 --known-sites $TMPDIR/vcf/dbsnp_138.hg19.vcf.gz \
 --known-sites $TMPDIR/vcf/hapmap_3.3.hg19.sites.vcf.gz \
 -O $TMPDIR/bam/${SID}_recal_data.table2

# load R/3.4.3 (needs ggplot2, gsalib packages) 
gatk --java-options "-Xmx8G" AnalyzeCovariates \
 -before $TMPDIR/bam/${SID}_recal_data.table1 \
 -after $TMPDIR/bam/${SID}_recal_data.table2 \
 -plots $TMPDIR/bam/${SID}_recal_plots.pdf

##########################################
# 3. Estimate cross-sample contamination #
##########################################

# using GetPileupSummaries and CalculateContamination 

# -V: A VCF file containing variants and allele frequencies
# -L: One or more genomic intervals over which to operate  This argument must be specified at least once. 
# Maximum population allele frequency of sites to consider.  Default value: 0.2.
# Minimum population allele frequency of sites to consider.  A low value increases accuracy at the expense of speed.  Default value: 0.01.
# Minimum read mapping quality  Default value: 50.
gatk --java-options "-Xmx8G" GetPileupSummaries \
 -I $TMPDIR/bam/${SID}.recal.bam \
 -V $TMPDIR/vcf/small_exac_common_3_hg19.vcf \
 -L $TMPDIR/bam/exome_intervals.list.bed \
 --maximum-population-allele-frequency 0.2 \
 --minimum-population-allele-frequency 0.01 \
 --min-mapping-quality 50 \
 -O $TMPDIR/vcf/${SID}.tumor_getpileupsummaries.table

gatk --java-options "-Xmx2G" CalculateContamination \
 -I $TMPDIR/vcf/${SID}.tumor_getpileupsummaries.table \
 -O $TMPDIR/vcf/${SID}.tumor_calculatecontamination.table
 
# delete uploaded or unwanted files
/bin/rm $TMPDIR/bam/${SID}.bam
/bin/rm $TMPDIR/vcf/dbsnp_138.hg19.vcf.gz*
/bin/rm $TMPDIR/vcf/hapmap_3.3.hg19.sites.vcf.gz*
/bin/rm $TMPDIR/vcf/af-only-gnomad.raw.sites.b37.vcf*
/bin/rm $TMPDIR/vcf/small_exac_common_3_b37.vcf*
/bin/rm $TMPDIR/vcf/small_exac_common_3_hg19.vcf*

####################################################################
# 4. Call somatic SNVs and indels via local assembly of haplotypes #
####################################################################

# In the case of a tumor with a matched normal control, we can exclude even rare germline variants and individual-specific artifacts.
# -tumor - SM field name from BAM file (use samtools view -H tumor.bam | grep '@RG')
# -bamout:String   File to which assembled haplotypes should be written
# -L: One or more genomic intervals over which to operate  This argument may be specified 0 or more times.

# Avoid calling any allele in the --germline-resource by setting --max-population-af to zero.
# Maximize the probability of calling any differing allele by setting --af-of-alleles-not-in-resource to zero.

# The PoN allows additional filtering of calls, e.g. those that arise from technical artifacts. 
# Therefore, it is important that the PoN consist of samples that are technically similar to and 
# representative of your tumor samples, e.g. that were sequenced on the same platform using the 
# same chemistry and analyzed using the same tool chain.

# Generate BAMOUTs with MuTect2 and manually review alignments with IGV
# --af-of-alleles-not-in-resource 0.0000025 \

# -bamout: File to which assembled haplotypes should be written

gatk --java-options "-Xmx16G" Mutect2 \
 --germline-resource $TMPDIR/vcf/af-only-gnomad.raw.sites.hg19.vcf \
 -R $TMPDIR/ref/genome.fa \
 -I $TMPDIR/bam/${SID}.recal.bam \
 -bamout $TMPDIR/bam/${SID}.bamout.bam \
 -tumor ${SID}_R1_001.fastq \
 -O $TMPDIR/vcf/${SID}.vcf \
 -L $TMPDIR/bam/exome_intervals.list.bed

##########################
# 5. Filter Mutect Calls #
##########################

gatk --java-options "-Xmx2G" FilterMutectCalls \
 -O $TMPDIR/vcf/${SID}.filtered.vcf \
 -V $TMPDIR/vcf/${SID}.vcf \
 --stats $TMPDIR/vcf/${SID}.Mutect2FilteringStats.tsv \
 --contamination-table $TMPDIR/vcf/${SID}.tumor_calculatecontamination.table

# count the passing calls with
# cat $TMPDIR/vcf/${SID}.filtered.vcf | grep -v '#' | grep 'PASS' | wc -l

#####################
# Variants To Table #
##################### 

# CHROM - 
# POS - 
# ID - 
# REF -
# ALT -
# FILTER:  
# POPAF: negative-log-10 population allele frequencies of alt alleles
# GT - Genotype
# AD - Allelic depths for the ref and alt alleles in the order listed
# AF - Allele fractions of alternate alleles in the tumor

gatk  --java-options "-Xmx2G" VariantsToTable \
 -R $TMPDIR/ref/genome.fa \
 -O $TMPDIR/vcf/${SID}.filtered.txt \
 -V $TMPDIR/vcf/${SID}.filtered.vcf \
 -F CHROM -F POS -F ID -F REF -F ALT -F FILTER -F POPAF -GF GT -GF AD -GF AF \
 -I $TMPDIR/bam/${SID}.recal.bam

################################
# Variant annotation - ANNOVAR # 
################################

module load annovar/201707

# Hard-filter variants
awk '/^#/||$7=="PASS"' $TMPDIR/vcf/${SID}.filtered.vcf > $TMPDIR/vcf/${SID}.pass.vcf
 
# annotate_variation.pl -downdb -webfrom annovar -buildver hg19 cosmic70 humandb/
# Annotate a VCF with annovar
# gene-based, region-based or filter-based annotations
convert2annovar.pl \
 -format vcf4 $TMPDIR/vcf/${SID}.pass.vcf > $TMPDIR/vcf/${SID}.avinput

annotate_variation.pl --geneanno --buildver hg19 -dbtype refGene $TMPDIR/vcf/${SID}.avinput ~/humandb/ \
 -out $TMPDIR/vcf/${SID}.geneanno.refGene_annovar

#filter rare or unreported variants (in 1000G/dbSNP) or predicted deleterious variants
annotate_variation.pl --filter --buildver hg19 -dbtype cosmic70 $TMPDIR/vcf/${SID}.avinput ~/humandb/ \
 -out $TMPDIR/vcf/${SID}.filter.cosmic70_annovar

 module unload annovar/201707

# germline resource and PoN replace the cosmic input
# Inputting the dbsnp file does nothing other than populate the rsid field.
# Germline resource , such as gnomAD, contains population allele frequencies of common and rare variants and used for somatic variant calling.
# GATK4 Mutect2 now takes a known population variants resource, e.g. gnomAD, with allele-specific frequencies.

# COSMIC data download
# https://www.biostars.org/p/297203/
# https://www.biostars.org/p/232994/

# library(devtools)
# source("http://bioconductor.org/biocLite.R") 
# biocLite("BSgenome.Hsapiens.UCSC.hg19")
# install_github("raerose01/deconstructSigs")

module unload R/3.4.3
module unload tabix/0.2.6

# delete raw data from TMPDIR   
/bin/rm $TMPDIR/bam/exome_intervals.list.bed

# copy results from TMPDIR
rsync -avr $TMPDIR/bam $OUT/
rsync -avr $TMPDIR/vcf $OUT/

# delete TMPDIR
/bin/rm -rf $TMPDIR
