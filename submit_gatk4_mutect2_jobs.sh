#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# ashok.reddy.dinasarapu@emory.edu              #
# Last updated on 03/07/2019                    #
#################################################

# Project Directory
DATA_HOME=$HOME/WES

# Script to run
SCRIPT=$DATA_HOME/scripts/gatk4_mutect2_job.sh

# bam files directory
BAM_DIR=$DATA_HOME/surecall_bam/BAM

threads=5

# get sample IDs from file names
list=$(ls $BAM_DIR/GHN-* | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | sort | uniq)

echo $list

COUNTER=0
JOB_ID=""

for file in $list; do

 COUNTER=$(( $COUNTER + 1 ))
 
 OUT=$DATA_HOME/GATK4_OUT/$file
 if [ ! -d $OUT ]; then
  mkdir -p $OUT
 fi

 echo "Job submitted for sample ${file} ..."
 BAM=$BAM_DIR/${file}*
 r=$(ls $BAM)
 echo $r

 # SID= echo ${file} | awk '{print substr($1, 1, length($1)-9)}'
 if [ $COUNTER -eq 1 ]; then
  qsub -v DATA_HOME=$DATA_HOME,SID=$file,R=$r,OUT=$OUT,t=$threads \
   -N ${file} \
   -q b.q \
   -pe smp $threads \
   -j y \
   -m abe \
   -M <your_email> \
   -cwd $SCRIPT
   JOB_ID=$file
 else
  qsub -hold_jid $JOB_ID -v DATA_HOME=$DATA_HOME,SID=$file,R=$r,OUT=$OUT,t=$threads \
   -N ${file} \
   -q b.q \
   -pe smp $threads \
   -j y \
   -m abe \
   -M <your_email> \
   -cwd $SCRIPT
   JOB_ID=$file
 fi
 sleep 1
done
